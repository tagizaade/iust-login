from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import requests
import time
import click

def internet_connected():
    try:
        status_code = requests.get('https://google.com').status_code
    except:
        return False
    return status_code == 200

def login(driver, username, password):
    print('trying to login')
    driver.get('https://its.iust.ac.ir/hotspot/login.php')
    time.sleep(5)
    driver.find_element_by_name("username").send_keys(username)
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_xpath('//button[@value="OK"]').click()
    print('logged in')


@click.command()
@click.option('--username', '-u')
@click.option('--password', '-p')
def main(username, password):
    count = 0
    driver = None
    
    while True:
        try:
            count += 1

            if not driver:
                driver = webdriver.Chrome()
                login(driver, username, password)

            if not internet_connected():
                login(driver, username, password)

            if count % 60 == 0:
                try:
                    driver.close()
                except Exception as e:
                    print(e)
                driver = None
                continue
            time.sleep(60)
        except Exception as e:
            print(e)
            time.sleep(10)

if __name__ == "__main__":
    main()