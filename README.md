# In the name of Allah

This is a script for automatic login to IUST internet in case if it was disconnected (User has been logged out).

First you need to install [selenium](https://www.selenium.dev/) and chrome driver for it.
Then install the requirements and enjoy the script.

## Usage:
```
python login.py -u username -p password
```